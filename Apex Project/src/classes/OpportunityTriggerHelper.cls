/*
One account have one Opportunity exist which is closed/won. now user will try to insert new opportunity under same account than populate following fields.
1.	Prior Opportunity(Lookup of Opporunity) – Last Opportunity Id
2.	Leadsource – From Last Opportunity
3.	Type = “Renewal”
4.	closedate = Lastopportunity + 1 year.

*/
/***********************************************
*Created By:Suraj Makandar
*Created Date:25-11-2015
*Modified By:
*Modified Date:
*Class Name:OpportunityTriggerHelper
*purpose:
*************************************************/
public  class OpportunityTriggerHelper {
    public void checkForOpportunities(){
        set<id> setOfIds =new set<id>();
        list<opportunity> ListOfOpp=trigger.new;
        list<opportunity> ListOfAllOpp=new list<opportunity>();
    //  map<String,AggregateResult> MapIdOpp=new map<String,AggregateResult>();
    map<id,Opportunity> MapIdOpp=new map<id,Opportunity>();
        list<opportunity> ListOfAllOpportunity=new list<opportunity>();
    
        for(opportunity InstanceOfOpp : ListOfOpp){
            setOfIds.add(InstanceOfOpp.AccountId);
        }
        if(!setOfIds.isEmpty()){
            ListOfAllOpportunity=[select id,AccountId,StageName,CloseDate,LeadSource,Type from opportunity 
            where StageName='Closed Won' And  AccountId in: setOfIds ];
        }
    //  list<opportunity> TempOppList=
        if(!ListOfAllOpportunity.isEmpty()){
            system.debug('Aggr All List is='+ListOfAllOpportunity);
            
        for(Opportunity InstanceOfOpp :ListOfAllOpportunity){
            if(MapIdOpp.Containskey(InstanceOfOpp.AccountID)){
                Date dt1,dt2;
                dt1=date.valueof(MapIdOpp.get(InstanceOfOpp.AccountID).get('CloseDate'));
                boolean b=dt1<InstanceOfOpp.CloseDate;
                if(b){
                    
                    //String str=String.valueof(InstanceOfOpp.get('Aid'));
                    MapIdOpp.put(InstanceOfOpp.AccountId,InstanceOfOpp);
                }
            }
            else{
                MapIdOpp.put(InstanceOfOpp.AccountId,InstanceOfOpp);
            }
            system.debug('Map is='+MapIdOpp);
          }
          if(!MapIdOpp.isEmpty()){
            for(Opportunity InstanceOfOpp :ListOfOpp){
                if(MapIdOpp.containsKey(InstanceOfOpp.AccountId)){
                //  InstanceOfOpp.addError('')
                    InstanceOfOpp.LeadSource=string.valueof(MapIdOpp.get(InstanceOfOpp.AccountId).get('LeadSource'));
                    InstanceOfOpp.Type=string.valueof(MapIdOpp.get(InstanceOfOpp.AccountId).get('Type'));
                    InstanceOfOpp.closedate =Date.valueof(MapIdOpp.get(InstanceOfOpp.AccountId).get('CloseDate'));
                    InstanceOfOpp.Old_Opportunity_Id__c=string.valueof(MapIdOpp.get(InstanceOfOpp.AccountId).get('Id'));
                    system.debug('Similar Opp is='+MapIdOpp.get(InstanceOfOpp.AccountId).get('LeadSource'));
                }
            }
          }
        }
        
    }
   }