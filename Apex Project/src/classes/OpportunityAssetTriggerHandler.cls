/******************************************************************************
*Created By:Suraj Makandar
*Created Date:
*Modified By:
*Modified Date:
*Class Name:TaskCountTriggerHandler
*purpose:Whenever opportunity getting closed create asset records how many products are related to opportunity.
  		 - Populate opportunity on Asset on custom lookup field
  		 - Populate Opportunity Lineitem id on custom text field

/*******************************************************************************/
public  class OpportunityAssetTriggerHandler {
    public void handlEvents(){
    	if(trigger.isAfter){
    	/*	if(trigger.isInsert){
    			OpportunityAssetTriggerHelper oppasst=new OpportunityAssetTriggerHelper();
    			oppasst.insertOppEvents(trigger.new);
    		}*/
    		if(trigger.isUpdate){
    			OpportunityAssetTriggerHelper oppasst=new OpportunityAssetTriggerHelper();
    			oppasst.UpdateOppEvents(trigger.new);
    		}
    	}
	}
}